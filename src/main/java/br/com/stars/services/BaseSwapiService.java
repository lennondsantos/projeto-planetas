package br.com.stars.services;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class BaseSwapiService {
	
	@Value("${project.configs.swapiUrl}")
    public String swapiUrl;
	
	@Value("${project.configs.swapiSearch}")
    public String swapiUrlFilter;
	
	public HttpEntity<String> setHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("user-agent", "SWAPI-Client/1.0");
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		return entity;
	}

	
	
}
