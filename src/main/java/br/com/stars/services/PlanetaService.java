package br.com.stars.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.stars.model.Planeta;
import br.com.stars.repository.PlanetaRepository;
import br.com.stars.services.exception.ObjetoNaoEncontradoException;
import br.com.stars.template.PlanetTemplate;

@Service
public class PlanetaService {
	
	@Autowired
	protected PlanetaRepository repository;
	
	@Autowired
	protected SwapiPlanetService swapiPlanetService; 
	
	public List<Planeta> buscarTodos(){
		List<Planeta> colabodores = repository.findAll();        
        return colabodores;

	}
	
	public Planeta buscarPor(String id) {
		Optional<Planeta> obj = repository.findById(id);
		if(!obj.isPresent()) {
			throw new ObjetoNaoEncontradoException("O planeta que você está procurando não existe");
		}
		
		return obj.get();
	}
	
	public void atualizar(Planeta planeta) {
		repository.save(planeta);
	}
	
	public void remover(String id) {
		repository.delete(this.buscarPor(id));		
	}
	
	public Planeta cadastrar(Planeta planeta) {
		planeta.setId(null);
		List<PlanetTemplate> result = swapiPlanetService.findPlanetByName(planeta.getNome());
		
		if(result != null && result.size() == 1) {
			planeta.setQtdAparicoesFilmes(result.iterator().next().getFilms().size());
		}
		return repository.save(planeta);
	}
	
	public List<Planeta> buscarPorNome(String nome) {
		List<Planeta> lista = repository.findByNomeLikeOrderByNome(nome);
		return lista;
	}

}
