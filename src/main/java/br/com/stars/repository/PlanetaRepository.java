package br.com.stars.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.stars.model.Planeta;

@Repository
public interface PlanetaRepository extends MongoRepository<Planeta, String>{
	
	List<Planeta> findByNomeLikeOrderByNome(String nome);

}
