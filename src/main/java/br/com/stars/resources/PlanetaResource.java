package br.com.stars.resources;

import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.stars.model.Planeta;
import br.com.stars.services.PlanetaService;


@RestController
@RequestMapping("/planetas")
public class PlanetaResource {
	
	@Autowired
	PlanetaService planetaService;
	
	@GetMapping
	public ResponseEntity<List<Planeta>> buscarTodos(){
		return ok(planetaService.buscarTodos());
	}


	@GetMapping("/{id}")
	public ResponseEntity<Planeta> buscarPorId(@PathVariable("id") String id){
		return ok(planetaService.buscarPor(id));
	}

	@PostMapping
	public ResponseEntity<Planeta> criar(@RequestBody Planeta planeta){

	    Planeta planetaNovo = planetaService.cadastrar(planeta);
		return created(fromCurrentRequest()
						.path("/{id}")
						.buildAndExpand(planetaNovo.getId())
						.toUri())
				.body(planetaNovo);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Void> atualizar(@PathVariable("id") String id,@RequestBody Planeta planeta){
		planeta.setId(id);
		planetaService.atualizar(planeta);
		return ok().build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deletar(@PathVariable("id") String id){
		planetaService.remover(id);
		return ok().build();
	}
	
	@GetMapping("nomes/{nome}")
	public ResponseEntity<List<Planeta>> buscarPorNome(@PathVariable("nome") String nome){
		return ok(planetaService.buscarPorNome(nome));
	}
}
