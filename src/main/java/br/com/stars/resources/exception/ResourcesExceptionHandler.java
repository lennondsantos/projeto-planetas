package br.com.stars.resources.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.stars.services.exception.ObjetoNaoEncontradoException;

@ControllerAdvice
public class ResourcesExceptionHandler {

	@ExceptionHandler(ObjetoNaoEncontradoException.class)
	public ResponseEntity<ErroPadrao> handleObjetoNaoEncontrado(ObjetoNaoEncontradoException e,HttpServletRequest request){
		ErroPadrao erro = new ErroPadrao(e.getMessage(), HttpStatus.NOT_FOUND.value());
		
		return ResponseEntity.status(erro.getStatus()).body(erro);
	}
	
}
