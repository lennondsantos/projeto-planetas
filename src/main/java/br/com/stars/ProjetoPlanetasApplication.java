package br.com.stars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoPlanetasApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoPlanetasApplication.class, args);
	}
	
}
